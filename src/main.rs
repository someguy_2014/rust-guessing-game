use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(0..10);

    loop {
        println!("Input your guess:");

        let mut guess = String::new();

        io::stdin().read_line(&mut guess).expect("Failed input.");

        if guess.trim() == "q" {
            break;
        }

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Invalid input, try again.");
                continue;
            }
        };

        println!("Your guess is: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("Correct number is: {}", secret_number);
                println!("You win!");
                break;
            }
        };
    }
}
